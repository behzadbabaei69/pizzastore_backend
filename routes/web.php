<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => \UriLocalizer::localeFromRequest(),
    'middleware' => 'localizeOur'], function () {

    Route::group(['namespace' => 'Front'], function () {
        Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
    });

});

Route::get('/admin', ['as' => 'admin', 'uses' => 'Admin\AdminController@index']);
