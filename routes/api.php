<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'API'], function () {

    Route::group(['prefix' => 'v1', 'namespace' => 'V1'], function () {

        Route::group(['as' => 'admin', 'prefix' => 'admin', 'namespace' => 'Admin'], function () {
            //auth
            Route::post('login', 'LoginController@login')->name('.login');
            Route::post('logout', 'LoginController@logout')->name('.logout');
            Route::post('check-token', 'LoginController@checkToken');

            Route::group(['prefix' => 'languages'], function () {
                Route::post('/', 'LanguageController@get');
                Route::post('/getEnabled', 'LanguageController@getEnabled');
                Route::post('/save', 'LanguageController@save');
                Route::post('/translations', 'LanguageController@getTranslations');
                Route::post('/translations/save', 'LanguageController@saveTranslation');
                Route::post('/translations/delete', 'LanguageController@DeleteTranslation');
            });

            Route::group(['prefix' => 'pizza'], function () {
                Route::post('/', 'PizzaController@get');
                Route::post('/save', 'PizzaController@save');
                Route::post('/delete', 'PizzaController@delete');
            });


            Route::group(['prefix' => 'currency'], function () {
                Route::post('/', 'CurrencyController@get');
                Route::post('save', 'CurrencyController@save');
                Route::post('delete', 'CurrencyController@delete');
            });

            Route::group(['prefix' => 'pizzaType'], function () {
                Route::post('/', 'PizzaTypeController@get');
                Route::post('/save', 'PizzaTypeController@save');
                Route::post('/delete', 'PizzaTypeController@delete');
            });


            Route::group(['prefix' => 'order'], function () {
                Route::post('/', 'OrderController@get');
                Route::post('/delete', 'OrderController@delete');
            });


        });

        Route::group(['as' => 'api.front', 'prefix' => 'front', 'namespace' => 'Front'], function () {

            Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
                Route::post('/register', [
                    'as' => '.register',
                    'uses' => 'RegisterController@register'
                ]);
            });

            Route::group(['prefix' => 'user'], function () {
                Route::get('/orders', 'UserController@getOrders');
            });

            Route::group(['prefix' => 'pizza'], function () {
                Route::get('/', 'PizzaController@get');
                Route::get('/{pizza}', 'PizzaController@show');
            });


            Route::group(['prefix' => 'config'], function () {
                Route::get('/', 'ConfigController@get');
            });


            Route::group(['prefix' => 'order'], function () {
                Route::get('/', 'OrderController@getOrders');
                Route::post('/', 'OrderController@store');
            });

            Route::group(['prefix' => 'currency'], function () {
                Route::get('/', 'CurrencyController@get');
                Route::get('/default', 'CurrencyController@getDefaultCurrency');
            });

            Route::group(['prefix' => 'pizzaType'], function () {
                Route::get('/', 'PizzaTypeController@get');
                Route::get('/{pizzaType}', 'PizzaTypeController@show');
            });
        });

    });

});
