<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CurrencyApiTest extends TestCase
{
    /**
     * A basic feature test example.
     * @group api-test
     * @group currency-api
     * @return void
     */
    public function testPizza()
    {
        $response = $this->get('/api/v1/front/currency');

        $response->assertStatus(200);

        $response->assertJson(['status' => 'success']);
    }
}
