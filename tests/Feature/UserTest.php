<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{

    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->faker = \Faker\Factory::create();
    }

    /**
     * @group user
     * @group user-register
     */
    public function testUserRegister()
    {
        $randomEmail = $this->faker->unique()->safeEmail;
        $response = $this->postJson('/api/v1/front/auth/register', [
            'email' => $randomEmail,
            'password' => '123456'
        ], ['Content-Type' => 'application/json;charset=UTF-8']);

        $response->assertStatus(200);
        $response->assertJson(['status' => 'success']);

        $this->assertDatabaseHas('users', ['email' => $randomEmail]);

        $stack = $response->decodeResponseJson(['data']);

    }

    /**
     * @group user
     * @depends testUserRegister
     */
    public function deleteUser($stack)
    {
        $this->assertIsArray($stack);
        $this->assertArrayHasKey('user');
        $user = $stack['user'];
        $this->assertNotNull($user);

        $this->assertDatabaseHas('users', ['email' => $user['email']]);
    }
}
