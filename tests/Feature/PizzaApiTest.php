<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PizzaApiTest extends TestCase
{
    /**
     * A basic feature test example.
     * @group api-test
     * @group pizza-api
     * @return void
     */
    public function testPizza()
    {
        $response = $this->get('/api/v1/front/pizza');

        $response->assertStatus(200);

        $response->assertJson(['status' => 'success']);
    }

    /**
     * A basic feature test example.
     * @group api-test
     * @group pizza-order-store-api
     */
    public function testOrderStore()
    {

        $jsonData = json_decode('{"orders":[{"id":"a915cbf0-e486-11ea-bcae-f173cd6a07fb","quantity":1,"totalPrice":10.4,"order":{"id":8,"name":"Buffalo Pizza","description":"Who says your pizza has to be strictly tomato-sauce based? Branch out with some buffalo sauce on your pie. All its spicy, salty, buttery goodness is a natural pairing for pizza.","imgUrl":"http://www.pizzastore.loc/uploads/pizzas/8/pizza8.png","thumbImgUrl":"http://www.pizzastore.loc/uploads/pizzas/8/small_pizza8.png","ingredient":"tomato sauce, mozzarella, mushrooms, ham, eggs, artichoke, cocktail sausages, green olives","defaultPizzaType":{"id":30,"pizza_size":2,"sizeName":"Medium","base_price":10.4,"fee":0,"slice":6,"order":2,"price":10.4,"isDefault":true},"pizzaTypes":[{"id":29,"pizza_size":1,"sizeName":"Small","base_price":8.96,"fee":0,"slice":4,"order":1,"price":8.96,"isDefault":false},{"id":30,"pizza_size":2,"sizeName":"Medium","base_price":10.4,"fee":0,"slice":6,"order":2,"price":10.4,"isDefault":true},{"id":31,"pizza_size":3,"sizeName":"Large","base_price":11.83,"fee":0,"slice":8,"order":3,"price":11.83,"isDefault":false},{"id":32,"pizza_size":4,"sizeName":"XLarge","base_price":13.27,"fee":0,"slice":10,"order":4,"price":13.27,"isDefault":false}]}}],"cart":{"totalCost":"10.40","cost":"10.40","totalItem":1,"deliveryCost":0,"totalFee":0,"is_delivery":false,"payment":1,"deliver_address":"","phone_number":"98999","contact_name":"43ssasasa"},"currency":"EUR"}',true);
        $response = $this->postJson('/api/v1/front/order', $jsonData,
            [
                'Content-Type' =>
                'application/json;charset=UTF-8'
            ]);

        $response->assertStatus(200);

        $response->assertJson(['status' => 'success']);
    }
}
