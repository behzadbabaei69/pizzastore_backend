<?php


namespace Tests\Unit;


use App\Repositories\UserRepository;
use Mockery;
use Tests\TestCase;
use Faker\Generator as Faker;

class UserTest extends TestCase
{
    /**
     * @var \App\Models\User|Mockery\LegacyMockInterface|Mockery\MockInterface
     */
    private $user;
    /**
     * @var Faker
     */
    private $faker;

    public function __construct()
    {
        parent::__construct();
        /*
         * The DbUserRepository depends on a User object, we let's mock that here.
         * I'm mocking the User object, instead of Eloquent, as User inherits
         * from Eloquent.
         */
        $this->user = Mockery::mock('App\Models\User');
        $this->faker = \Faker\Factory::create();
    }

    /**
     * @group user
     * @group user-register
     */
    public function testUserRegister()
    {

        $repository = new UserRepository($this->user);

        $randomEmail = $this->faker->unique()->safeEmail;
        $user = $repository->registerUser(['email' => $randomEmail, 'password' => '123456']);
        $this->assertNotNull($user);;
        $this->assertDatabaseHas('users', ['email' => $randomEmail]);

        $repository->deleteUser($user->id);
        $this->assertDatabaseMissing('users', ['email' => $randomEmail]);

    }
}
