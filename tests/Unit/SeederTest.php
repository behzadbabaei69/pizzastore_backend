<?php

namespace Tests\Unit;

use App\Models\Pizza;
use App\Models\PizzaType;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Tests\TestCase;

class SeederTest extends TestCase
{

    /**
     * @group test-seeders
     * @group pizza-seeder
     */
    public function testPizzaSeeder()
    {

        $pizzasJson = file_get_contents(base_path() . '/source_files/pizzas/pizzas.json');

        $pizzaSeeds = json_decode($pizzasJson);

        $this->assertJson($pizzasJson);

        $pizzas = Pizza::all();

        if (!File::exists(public_path('uploads/pizzas'))) {
            File::makeDirectory(public_path('uploads/pizzas'), 0777);
        }

        $pizzasFolder = File::directories(public_path('uploads/pizzas/'));


        foreach ($pizzasFolder as $folder) {
            $deleteStatus = File::deleteDirectory($folder);
        }

        foreach ($pizzas as $pizza) {
            if (File::isDirectory(public_path('uploads/pizzas/' . $pizza->id))) {
                $deleteStatus = File::deleteDirectory(public_path('uploads/pizzas/' . $pizza->id));
            }
        }

        \DB::table('pizzas')->truncate();
        \DB::table('pizza_types')->truncate();


        \Log::info("previous data were deleted");

        foreach ($pizzaSeeds as $seed) {
            $pizza = new Pizza();
            $pizza->name = $seed->name;
            $pizza->img = $seed->img;
            $pizza->description = $seed->description;
            $pizza->ingredient = $seed->ingredient;

            $pizza->save();

            File::makeDirectory(public_path('uploads/pizzas/' . $pizza->id));
            //if directory is created!

            $srcFile = base_path() . '/source_files/pizzas/images/' . $pizza->img;
            $destFile = public_path('uploads/pizzas/' . $pizza->id . '/' . $pizza->img);
            $status = copy($srcFile, $destFile);


            if ($status) {
                $smallThumbnail = 'small_' . $pizza->img;
                $smallThumbnailPath = public_path('uploads/pizzas/' . $pizza->id . '/' . $smallThumbnail);

                $statusThumbnail = copy($srcFile, $smallThumbnailPath);
                $this->assertFileExists($smallThumbnailPath);

                if ($statusThumbnail) {
                    $this->createThumbnail($smallThumbnailPath, 300, 300);
                }
            }


            $pizzaSizes = [
                PizzaType::PIZZA_SMALL => ['name' => 'Small', 'slice' => 4, 'order' => 1],
                PizzaType::PIZZA_MEDIUM => ['name' => 'Medium', 'slice' => 6, 'order' => 2],
                PizzaType::PIZZA_LARGE => ['name' => 'Large', 'slice' => 8, 'order' => 3],
                PizzaType::PIZZA_XLARGE => ['name' => 'XLarge', 'slice' => 10, 'order' => 4]
            ];
        }


        $this->assertDatabaseHas('pizzas', ['id' => 1]);

        $pizzas = Pizza::all();
        foreach ($pizzas as $pizza) {
            $rand = (rand(50, 100) / 10);
            foreach ($pizzaSizes as $key => $size) {
                $ratio = 0.78;
                $pizzaType = new PizzaType();
                $pizzaType->pizza_id = $pizza->id;
                $pizzaType->pizza_size = $key;
                $pizzaType->base_price = ($rand + floatval($ratio * ($size['slice'])));
                $pizzaType->fee = 0;
                $pizzaType->slice = $size['slice'];
                $pizzaType->order = $size['order'];
                $pizzaType->save();

            }
        }

        $this->assertDatabaseHas('pizza_types', ['id' => 1]);
        $this->assertDatabaseHas('pizza_types', ['pizza_id' => 1]);

        \Log::info("Seeding is done!");

    }

    public function createThumbnail($path, $width, $height)
    {
        $img = Image::make($path)->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($path);
    }


    /**
     * @group test-seeders
     */
    public function testMainPizzaSeeder()
    {
        \Illuminate\Support\Facades\Artisan::call('db:seed --class=PizzaSeeder');

        $this->assertDatabaseHas('pizzas', ['id' => 1]);
        $this->assertDatabaseHas('pizzas', ['name' => 'Capricciosa']);

    }

    /**
     * @group test-seeders
     * @group test-currency-seeder
     */
    public function testCurrencySeeder()
    {
        \Illuminate\Support\Facades\Artisan::call('db:seed --class=CurrencySeeder');

        $this->assertDatabaseHas('currencies', ['code' => 'USD']);
    }

}
