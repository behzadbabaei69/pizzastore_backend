<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{

    const DISABLED  = 0;
    const ENABLED   = 1;

    protected $table = 'translator_languages';
    protected $guarded = [];
    protected $appends = ['imgUrl'];

    public function getImgUrlAttribute()
    {
        return asset('img/languages/' . $this->locale . '.svg');
    }

}
