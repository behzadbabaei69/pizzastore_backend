<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


/**
 * Class Pizza
 * @package App\Models
 *
 * @property int id
 * @property string name
 * @property string description
 * @property string ingredient
 * @property string img
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class Pizza extends Model
{

    protected $fillable = ['name', 'description', 'ingredient', 'img'];

    protected $appends = ['imgUrl', 'thumbImgUrl'];

    protected $hidden = ['created_at', 'updated_at'];


    public function getImgUrlAttribute()
    {
        return asset('uploads/pizzas/' . $this->id . '/' . $this->img);
    }

    public function getThumbImgUrlAttribute()
    {
        return asset('uploads/pizzas/' . $this->id . '/small_' . $this->img);
    }

    public function pizzaTypes()
    {
        return $this->hasMany(PizzaType::class, 'pizza_id');
    }

    public function getDefaultPizzaType()
    {
        return $this->pizzaTypes()
            ->where('pizza_size',PizzaType::PIZZA_DEFAULT_TYPE)
            ->first();
    }

}
