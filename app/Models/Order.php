<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


/**
 * Class Order
 * @package App\Models
 *
 * @property int id
 * @property integer user_id
 * @property float total_price
 * @property float total_fee
 * @property float total_discount
 * @property float delivery_cost
 * @property boolean is_delivery
 * @property string delivery_address
 * @property string tracking_number
 * @property string phone_number
 * @property string contact_name
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class Order extends Model
{

    protected $appends = ['totalCost'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class, 'order_id');
    }


    public function getTotalCostAttribute()
    {
        $totalCost = $this->total_price;
        if ($this->is_delivery) {
            $totalCost = $totalCost + $this->delivery_cost;
        }
        return $totalCost;
    }
}
