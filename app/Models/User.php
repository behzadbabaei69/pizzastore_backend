<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, HasRoles;


//    protected $guard_name = 'web';

    const ROLE_CUSTOMER = 1;
    const ROLE_ADMIN = 2;
    const ROLE_SUPPLIER = 3;
    const ROLE_GUEST = 4;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'role',
        'role_id',
        'api_token',
        'address',
        'phone_number'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id');
    }

    public function scopeAdmin(Builder $query)
    {
        return $query->where('role', self::ROLE_ADMIN);
    }

    public function isAdmin()
    {
        return $this->role == self::ROLE_ADMIN;
    }


}
