<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


/**
 * Class PizzaType
 * @package App\Models
 *
 * @property int id
 * @property integer pizza_id
 * @property float base_price
 * @property integer slice
 * @property integer pizza_size
 * @property integer order
 * @property float fee
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class PizzaType extends Model
{
    const PIZZA_SMALL = 1;
    const PIZZA_MEDIUM = 2;
    const PIZZA_LARGE = 3;
    const PIZZA_XLARGE = 4;

    const PIZZA_DEFAULT_TYPE = self::PIZZA_MEDIUM;

    const PIZZA_SIZES = [
        self::PIZZA_SMALL => 'Small',
        self::PIZZA_MEDIUM => 'Medium',
        self::PIZZA_LARGE => 'Large',
        self::PIZZA_XLARGE => 'XLarge',
    ];

    protected $fillable = ['pizza_id', 'base_price', 'slice', 'pizza_size', 'order', 'fee'];
    protected $appends = ['sizeName', 'price', 'isDefault'];
    protected $hidden = ['created_at', 'updated_at'];

    public function pizza()
    {
        return $this->belongsTo(Pizza::class, 'pizza_id');
    }

    public function getSizeNameAttribute()
    {
        return self::PIZZA_SIZES[$this->pizza_size];
    }

    public function getPriceAttribute()
    {
        $price = number_format((($this->base_price + $this->fee) - ($this->discount)), 2);
        return (float)($price);
    }

    public function getIsDefaultAttribute()
    {
        return ($this->pizza_size == self::PIZZA_DEFAULT_TYPE);
    }



}
