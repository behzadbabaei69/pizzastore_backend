<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Currency
 * @package App\Models
 *
 * @property int id
 * @property string name
 * @property string code
 * @property string symbol
 * @property string format
 * @property string exchange_rate
 * @property bool active
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class Currency extends Model
{
    protected $guarded = [];


    public function scopeActive(Builder $query)
    {
        return $query->where('active', true);
    }


}
