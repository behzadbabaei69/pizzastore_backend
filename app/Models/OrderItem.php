<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{

    protected $fillable = [
        'order_id',
        'orderable_type',
        'orderable_id',
        'quantity',
        'price',
        'discount',
        'fee'
    ];
    public function order()
    {
        return $this->belongsTo(Order::class,'order_id');
    }

    public function orderable()
    {
        return $this->morphTo();
    }
}
