<?php

namespace App\Repositories;

use App\Models\Currency;

class CurrencyRepository extends Repository
{

    /**
     * CurrencyRepository constructor.
     * @param Currency $currency
     */
    public function __construct(Currency $model){
        $this->model = $model;
        $this->query = $model::query();
    }

    public function get($filters = [])
    {
        if(isset($filters['take'])){
            $this->query->take((int)$filters['take'])->skip((int)$filters['skip']);
        }
        $this->query->orderByDesc('created_at');
        $items = $this->query->get();
        return $items;
    }


}
