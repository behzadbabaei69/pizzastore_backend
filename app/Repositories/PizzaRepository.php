<?php

namespace App\Repositories;

use App\Models\Pizza;

class PizzaRepository extends Repository
{

    /**
     * PizzaRepostiory constructor.
     * @param Pizza $pizza
     */
    public function __construct(Pizza $model){
        $this->model = $model;
        $this->query = $model::query();
    }

    public function get($filters = [])
    {
        if(isset($filters['take'])){
            $this->query->take((int)$filters['take'])->skip((int)$filters['skip']);
        }
        $this->query->with('pizzaTypes');
        $this->query->orderByDesc('created_at');
        $items = $this->query->get();
        return $items;
    }


}
