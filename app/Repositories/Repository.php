<?php

namespace App\Repositories;

abstract class Repository
{
    protected $model = null;
    protected $query = null;

    public abstract function get($filters = []);
}