<?php

namespace App\Repositories;

use App\Models\PizzaType;

class PizzaTypeRepository extends Repository
{

    /**
     * PizzaTypeRepository constructor.
     * @param PizzaType $pizzaType
     */
    public function __construct(PizzaType $model){
        $this->model = $model;
        $this->query = $model::query();
    }

    public function get($filters = [])
    {
        if(isset($filters['take'])){
            $this->query->take((int)$filters['take'])->skip((int)$filters['skip']);
        }
        $this->query->with('pizza');
        $this->query->orderByDesc('created_at');
        $items = $this->query->get();
        return $items;
    }


}
