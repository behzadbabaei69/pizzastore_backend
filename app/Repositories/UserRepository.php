<?php

namespace App\Repositories;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserRepository extends Repository
{

    /**
     * UserRepository constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->model = $user;
    }


    public function get($filters = [])
    {
        if (isset($filters['take'])) {
            $this->query->take((int)$filters['take'])->skip((int)$filters['skip']);
        }
        $this->query->orderByDesc('created_at');
        $items = $this->query->get();
        return $items;
    }

    public function findByApiToken($apiToken)
    {
        return User::where('api_token', $apiToken)->with('orders')->first();
    }

    public function registerUser($data = [])
    {
        $password = $data['password'];
        //check if user exist with this api token
        $apiToken = $data['api_token'] ?? Str::random(30);
        $user = User::where('api_token', $apiToken)->first();
        if (!$user) {
            $user = new User();
        }
        $user->first_name = $data['first_name'] ?? '';
        $user->last_name = $data['last_name'] ?? '';
        $user->phone_number = $data['phone_number'] ?? '';
        $user->email = $data['email'];
        $user->password = Hash::make($password);
        $user->role = User::ROLE_GUEST;
        $user->api_token = $apiToken;
        $user->save();

        return ($user) ? $user : null;
    }

    public function getUserOrders(User $user)
    {
        $orders = $user->orders()->get();
        return $orders;
    }

    public function deleteUser($id)
    {
        $user = User::find($id);

        if ($user) {
            $user->delete();
        }
    }
}
