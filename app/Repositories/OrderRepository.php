<?php

namespace App\Repositories;

use App\Models\Order;

class OrderRepository extends Repository
{

    /**
     * OrderRepository constructor.
     * @param Order $order
     */
    public function __construct(Order $model){
        $this->model = $model;
        $this->query = $model::query();
    }

    public function get($filters = [])
    {
        if(isset($filters['take'])){
            $this->query->take((int)$filters['take'])->skip((int)$filters['skip']);
        }
        $this->query->with('orderItems');
        $this->query->orderByDesc('created_at');
        $items = $this->query->get();
        return $items;
    }


}
