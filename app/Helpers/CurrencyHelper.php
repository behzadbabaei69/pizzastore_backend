<?php


namespace App\Helpers;


class CurrencyHelper
{

    public static function getCurrencyValue($amount = 0.0, $currency = 'USD', $format = false)
    {
        return number_format(currency($amount, 'USD', strtoupper($currency), $format), 2);
    }
}