<?php

namespace App\Http\Resources\V1;

use App\Helpers\CurrencyHelper;
use Illuminate\Http\Resources\Json\JsonResource;

class PizzaType extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $currency = $request->currency ?? 'USD';
        return [
            'id' => $this->id,
            'pizza_size' => $this->pizza_size,
            'sizeName' => $this->sizeName,
            'base_price' => (float)CurrencyHelper::getCurrencyValue($this->base_price, $currency),
            'fee' => (float)CurrencyHelper::getCurrencyValue($this->fee, $currency),
            'slice' => $this->slice,
            'order' => $this->order,
            'price' => (float)CurrencyHelper::getCurrencyValue($this->price, $currency),
            'isDefault' => $this->isDefault
        ];
    }
}
