<?php

namespace App\Http\Resources\V1;

use App\Helpers\CurrencyHelper;
use Illuminate\Http\Resources\Json\JsonResource;

class Order extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $currency = $request->currency ?? 'USD';
        return [
            'id' => $this->id,
            'total_price' => (float)CurrencyHelper::getCurrencyValue($this->total_price, $currency),
            'total_cost' => (float)CurrencyHelper::getCurrencyValue($this->totalCost, $currency),
            'total_fee' => (float)CurrencyHelper::getCurrencyValue($this->total_fee, $currency),
            'delivery_cost' => (float)CurrencyHelper::getCurrencyValue($this->delivery_cost, $currency),
            'total_discount' => (float)CurrencyHelper::getCurrencyValue($this->total_discount, $currency),
            'is_delivery' => $this->is_delivery,
            'tracking_number' => $this->tracking_number,
            'order_items' => $this->orderItems,
            'delivery_address' => $this->delivery_address,
            'phone_number' => $this->phone_number,
            'contact_name' => $this->contact_name,
        ];
    }
}
