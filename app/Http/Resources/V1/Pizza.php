<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\V1\PizzaType as PizzaTypeResource;

class Pizza extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'imgUrl' => $this->imgUrl,
            'thumbImgUrl' => $this->thumbImgUrl,
            'ingredient' => $this->ingredient,
            'defaultPizzaType' => new PizzaTypeResource($this->getDefaultPizzaType()),
            'pizzaTypes' => PizzaTypeResource::collection($this->pizzaTypes)
        ];
    }
}
