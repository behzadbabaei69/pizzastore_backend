<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index(Request $request)
    {
        if($request->has('currency')){
            if (currency()->isActive(strtoupper($request->currency))) {
                session(['currency' => strtoupper($request->currency)]);
            }
        }

        return redirect()->route('admin');
//        return view('front.index');
    }

    public function theme()
    {
        return view('front.theme');
    }

}
