<?php

namespace App\Http\Controllers\API\V1\Front;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\FrontApiBaseController;
use App\Models\Pizza;
use App\Repositories\PizzaRepository;
use Illuminate\Http\Request;
use \App\Http\Resources\V1\Pizza as PizzaResource;

class PizzaController extends FrontApiBaseController
{

    /**
     * @var PizzaRepository
     */
    private $pizzaRepository;

    public function __construct(PizzaRepository $pizzaRepository)
    {
        parent::__construct();
        $this->pizzaRepository = $pizzaRepository;
    }

    public function get(Request $request)
    {
        $pizzas = $this->pizzaRepository->get($request->all());
        return $this->responseSuccess([
            'pizzas' => PizzaResource::collection($pizzas),
            'total' => $pizzas->count(),
        ]);
    }

    public function show(Pizza $pizza)
    {
        return $this->responseSuccess([
            'pizza' => new PizzaResource($pizza),
        ]);
    }

}
