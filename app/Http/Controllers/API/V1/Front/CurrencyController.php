<?php

namespace App\Http\Controllers\API\V1\Front;

use App\Http\Controllers\FrontApiBaseController;
use App\Http\Resources\V1\Currency as CurrencyResource;
use App\Repositories\CurrencyRepository;
use Illuminate\Http\Request;

class CurrencyController extends FrontApiBaseController
{

    /**
     * @var CurrencyRepository
     */
    private $currencyRepository;

    public function __construct(CurrencyRepository $currencyRepository)
    {
        parent::__construct();
        $this->currencyRepository = $currencyRepository;
    }

    public function get(Request $request)
    {
        $pizzas = $this->currencyRepository->get($request->all());
        return $this->responseSuccess([
            'currencies' => CurrencyResource::collection($pizzas),
            'total' => $pizzas->count(),
        ]);
    }


    public function getDefaultCurrency(Request $request)
    {
        if ($request->has('currency')) {
            $currency = currency()->getCurrency(strtoupper($request->currency));
        } else {
            $currency = currency()->getCurrency('USD');
        }

        return $this->responseSuccess([
            'defaultCurrency' => new CurrencyResource(json_decode(json_encode($currency))),
        ]);
    }
}
