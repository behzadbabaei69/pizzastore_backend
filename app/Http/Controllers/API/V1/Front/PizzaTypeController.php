<?php

namespace App\Http\Controllers\API\V1\Front;

use App\Http\Controllers\Admin\AdminController;

use App\Http\Controllers\FrontApiBaseController;
use App\Models\PizzaType;
use App\Repositories\PizzaTypeRepository;
use Illuminate\Http\Request;
use \App\Http\Resources\V1\PizzaType as PizzaTypeResource;

class PizzaTypeController extends FrontApiBaseController
{
    /**
     * @var PizzaTypeRepository
     */
    private $pizzaTypeRepository;

    public function __construct(PizzaTypeRepository $pizzaTypeRepository)
    {
        parent::__construct();
        $this->pizzaTypeRepository = $pizzaTypeRepository;
    }

    public function get(Request $request)
    {
        $pizzaTypes = $this->pizzaTypeRepository->get($request->all());
        return $this->responseSuccess([
            'pizzaTypes' => PizzaTypeResource::collection($pizzaTypes),
            'total' => $pizzaTypes->count(),
        ]);
    }

    public function show(PizzaType $pizzaType)
    {
        return $this->responseSuccess([
            'pizzaType' => new PizzaTypeResource($pizzaType),
        ]);
    }

}
