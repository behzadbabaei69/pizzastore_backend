<?php


namespace App\Http\Controllers\API\V1\Front\Auth;


use App\Http\Controllers\FrontApiBaseController;
use Illuminate\Support\Facades\Validator;
use App\Repositories\UserRepository;
use App\Http\Resources\V1\User as UserResource;
use Exception;
use Illuminate\Http\Request;


class RegisterController extends FrontApiBaseController
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * RegisterController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        parent::__construct();
        $this->userRepository = $userRepository;
    }

    public function register(Request $request)
    {

        try {

            $validator = Validator::make($request->all(), [
                'email' => 'required|min:6|unique:users,email',
                'password' => 'required|min:6'
            ], [
                'email.required' => 'email or username is required',
                'email.unique' => 'email or username should be unique',
                'email.min' => 'email or username should be at least 6 character',
                'password.required' => 'password is required',
                'password.min' => 'password should be at least 6 character'
            ]);

            if ($validator->fails()) {
                return $this->responseError($validator->errors()->first());
            }

            $data = $request->only(
                'first_name',
                'last_name',
                'phone_number',
                'email',
                'password',
                'api_token'
            );

            $user = $this->userRepository->registerUser($data);

            if (!$user) {
                return $this->responseError("User cannot be registered!");
            }

            return $this->responseSuccess([
                'user' => new UserResource($user),
            ], 'The user is registered successfully!');

        } catch (Exception $e) {
            return $this->responseError("Cannot process your request at the moment!");
        }
    }
}
