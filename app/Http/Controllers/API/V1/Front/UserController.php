<?php


namespace App\Http\Controllers\API\V1\Front;


use App\Http\Controllers\FrontApiBaseController;
use App\Http\Resources\V1\Order as OrderResource;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class UserController extends FrontApiBaseController
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        parent::__construct();
        $this->userRepository = $userRepository;
    }

    public function getOrders(Request $request)
    {


        if ((!$request->hasHeader('ApiToken')) || (is_null($request->header('ApiToken')))) {
            return $this->responseError('Ordering without token is not possible!');
        }

        $apiToken = $request->header('ApiToken');

        $user = $this->userRepository->findByApiToken($apiToken);

        $orders = [];
        if ($user) {
            $orders = $user->orders;
        }

        return $this->responseSuccess([
            'orders' => OrderResource::collection($orders)
        ]);
    }
}
