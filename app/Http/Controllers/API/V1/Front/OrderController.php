<?php

namespace App\Http\Controllers\API\V1\Front;

use App\Helpers\StringHelper;
use \App\Http\Controllers\FrontApiBaseController;
use App\Http\Requests\StoreOrderApiRequest;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Pizza;
use App\Models\PizzaType;
use App\Models\User;
use App\Repositories\UserRepository;
use Exception;


class OrderController extends FrontApiBaseController
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function __construct(UserRepository $userRepository)
    {
        parent::__construct();

        $this->userRepository = $userRepository;
        $this->faker = \Faker\Factory::create();
    }


    public function store(StoreOrderApiRequest $request)
    {
        \DB::beginTransaction();
        try {
            $data = $request->only('orders', 'cart', 'currency');

            if ($request->hasHeader('ApiToken')) {
                $this->responseError('Ordering without token is not possible!');
            }

            $apiToken = $request->header('ApiToken');
            //search for users
            $user = User::where('api_token', $apiToken)->first();
            if (!$user) {
                //then create a user
                $randomEmail = $this->faker->unique()->safeEmail;
                $user = $this->userRepository->registerUser([
                    'email' => $randomEmail,
                    'password' => $randomEmail,
                    'api_token' => $apiToken
                ]);
            }

            if (!$user) {
                $this->responseError('User not found!');
            }


            $cost = 0.0;
            $totalItems = 0;
            $deliveryCost = 0.0;

            $newOrder = new Order();
            $newOrder->user_id = $user->id;
            $newOrder->total_price = $cost;
            $newOrder->total_fee = 0;
            $newOrder->total_discount = 0;
            $newOrder->is_delivery = ($data['cart']['is_delivery']) ?? false;
            $newOrder->phone_number = ($data['cart']['phone_number']) ?? '';
            $newOrder->contact_name = ($data['cart']['contact_name']) ?? '';
            $newOrder->delivery_address = ($data['cart']['deliver_address']) ?? '';
            $newOrder->save();


            foreach ($data['orders'] as $order) {
                //get each item and quantity --
                $pizza = Pizza::find($order['order']['id']);
                if ($pizza) {
                    $orderdPizzaType = PizzaType::find($order['order']['defaultPizzaType']['id']);
                    $price = ($orderdPizzaType->base_price * $order['quantity']);
                    $totalItems += $order['quantity'];
                    $cost += $price;

                    OrderItem::create([
                        'orderable_type' => 'App\Models\PizzaType',
                        'order_id' => $newOrder->id,
                        'orderable_id' => $orderdPizzaType->id,
                        'quantity' => $order['quantity'],
                        'price' => $orderdPizzaType->price,
                        'discount' => 0,
                        'fee' => 0,
                    ]);
                }
            }

            $cost = round($cost, 2);

            if (($data['cart']['is_delivery'])) {
                $deliveryCost = round(($cost / 10), 2);
            }

            $newOrder->total_price = $cost;
            $newOrder->delivery_cost = $deliveryCost;
            $newOrder->total_fee = 0;
            $newOrder->tracking_number = StringHelper::generateRandomString();
            $newOrder->save();

            \DB::commit();

            return $this->responseSuccess(
                ['order' => $newOrder],
                'Your order is successfully stored with us! we will contact you soon!'
            );
        } catch (Exception $e) {
            \DB::rollback();
//            return $this->responseError($e->getMessage());
           return $this->responseError("Cannot process your request at the moment!");
        }

    }
}
