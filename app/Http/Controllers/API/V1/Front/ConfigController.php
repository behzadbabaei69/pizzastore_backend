<?php

namespace App\Http\Controllers\API\V1\Front;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\FrontApiBaseController;
use App\Http\Resources\V1\Currency;
use Illuminate\Http\Request;

class ConfigController extends FrontApiBaseController
{

    public function get(Request $request)
    {
        if ($request->has('currency')) {
            $currency = currency()->getCurrency(strtoupper($request->currency));
        } else {
            $currency = currency()->getCurrency('USD');
        }

        return $this->responseSuccess([
            'defaultCurrency' => new Currency(json_decode(json_encode($currency))),
            'user' => null,
            'user_token' => null,
            'base_url' => route('home'),
            'api_prefix' => '/api/v1/front/'
        ]);
    }

}
