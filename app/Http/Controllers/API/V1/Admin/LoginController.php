<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Admin\AdminController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Permission;

class LoginController extends AdminController
{
//    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function checkToken()
    {
        /** @var User $user */
        $user = auth('api')->user();
        if (!$user) {
            return response()->json(['message' => 'Unauthorized', 'status' => 'error']);
        }
//        if (!$user->isAdmin()) {
//            return response()->json(['message' => 'You have no rights', 'status' => 'error']);
//        }

        $permissions = $user->getAllPermissions();
        $userPermissions = [];
        foreach ($permissions as $permission) {
            $userPermissions[] = $permission->name;
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Token accepted',
            'user' => $user,
            'userPermissions' => $userPermissions,
        ]);
    }

    public function login(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'email' => 'required|exists:users,email',
                'password' => 'required'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }

        $userExist = User::where('email', $request->email)->first();

        if (!$userExist->isAdmin()) {
            return response()->json(['message' => 'You have no rights', 'status' => 'error']);
        }

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

            $user = Auth()->user();

            $tokenResult = $user->createToken(env('APP_NAME'));

            $permissions = $user->getAllPermissions();
            $userPermissions = [];
            foreach ($permissions as $permission) {
                $userPermissions[] = $permission->name;
            }
            // The user is being remembered...

            $loginData = [
                'status' => 'success',
                'message' => 'Successfully login',
                'user' => $user,
                'userPermissions' => $userPermissions,
                'accessToken' => $tokenResult->accessToken,
                //'tokenType' => 'Bearer',
                //'expiresAt' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()
            ];

            return response()->json($loginData, 200);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'invalid user credentials',
            ], 200);
        }
    }

    protected function sendLoginResponse(Request $request)
    {

        $user = $request->user();
        if (!$user->isAdmin()) {
            return response()->json(['message' => 'You have no rights', 'status' => 'error']);
        }
        $tokenResult = $user->createToken(env('APP_NAME'));

        $permissions = $user->getAllPermissions();
        $userPermissions = [];
        foreach ($permissions as $permission) {
            $userPermissions[] = $permission->name;
        }

        return $this->authenticated($request, $this->guard()->user())
            ?: response()->json([
                'status' => 'success',
                'message' => 'Successfully login',
                'user' => $user,
                'userPermissions' => $userPermissions,
                'accessToken' => $tokenResult->accessToken,
                //'tokenType' => 'Bearer',
                //'expiresAt' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()
            ], 200);
    }

    protected function sendFailedLoginResponse()
    {
        return response()->json(['message' => 'These credentials do not match our records.', 'status' => 'error']);
    }

    public function logout()
    {
        if (!auth('api')->user()) {
            return response()->json(['message' => 'Unauthorized', 'status' => 'error']);
        }
        auth('api')->user()->token()->revoke();
        return response()->json(['message' => 'Successfully logged out', 'status' => 'success']);
    }
}
