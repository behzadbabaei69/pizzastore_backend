<?php


namespace App\Http\Controllers\API\V1\Admin;


use App\Http\Controllers\Admin\AdminController;
use App\Models\Order;
use App\Repositories\OrderRepository;
use Illuminate\Http\Request;

class OrderController extends AdminController
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        parent::__construct();
        $this->orderRepository = $orderRepository;
    }

    public function get(Request  $request)
    {
        $orders = $this->orderRepository->get($request->all());
        return $this->responseSuccess([
            'orders' => $orders,
            'total' => $orders->count(),
        ]);
    }

    public function delete(Request $request)
    {
        $id = $request->get('id');
        $order = Order::find($id);
        $order->orderItems()->delete();
        $order->delete();
        return $this->responseSuccess(['status' => 'success', 'message' => 'Record deleted successfully']);
    }
}
