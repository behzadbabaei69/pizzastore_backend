<?php

namespace App\Http\Controllers\API\V1\Admin;


use App\Http\Controllers\Admin\AdminController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends AdminController
{
    public function get(Request $request)
    {
        $query = User::query();

        if ($request->email) {
            $query->where('email', 'like', '%' . $request->email . '%');
        }

        $allRecords = $query->get();

        $query->take((int)$request->take)->skip((int)$request->skip);
        $users = $query->get();
        foreach ($users as $user) {
            $user->first_name .= ' ' . $user->last_name;
            $user->registerDate = $user->created_at->format('d/m/Y');
            unset($user->last_name, $user->created_at);
        }
        return response()->json([
            'users' => $users,
            'total' => $allRecords->count(),
        ]);
    }


    public function getAdmins()
    {
        $users = User::admin()
            ->with(['country', 'mainRole'])
            ->get();
        foreach ($users as $user) {
            $user->name = trim($user->first_name .= ' ' . $user->lastname);
            $user->registerDate = $user->created_at->format('d/m/Y');
        }
        return response()->json([
            'users' => $users,
        ]);
    }

    public function getById(Request $request)
    {
        if (!$request->has('id')) {
            return response()->json(['message' => 'Missing required parameter id']);
        }
        $user = User::find($request->id);
        if (!$user) {
            return response()->json(['message' => 'User not found']);
        }
        return response()->json($user);
    }

    public function edit(Request $request)
    {
        $input = $request->only('id', 'first_name', 'last_name');
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|min:1',
            'first_name' => 'required|integer|min:0',
            'last_name' => 'required|integer|min:0'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }
        /** @var User $user */
        $user = User::find($input['id']);
        if (!$user) {
            return response()->json(['status' => 'error', 'message' => 'User not found']);
        }

        //Balance history

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->update();
        return response()->json(['status' => 'success', 'message' => 'Record updated successfully']);
    }

    public function saveAdmin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'string|max:255',
            'last_name' => 'string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => 'sometimes|required|string|min:4|confirmed'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }

        $user = User::find($request->id);
        if (!$user) {
            if (!$request->has('password')) {
                return response()->json(['status' => 'error', 'message' => 'Please enter password']);
            }
            if (User::where('email', $request->email)->first()) {
                return response()->json(['status' => 'error', 'message' => 'This email already exists']);
            }
            $user = new User();
        }
        $data = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'role' => User::ROLE_ADMIN,
        ];
        if ($request->password) {
            $data['password'] = Hash::make($request->password);
        }
        $user->fill($data)->save();

        //find role
//        $role = Role::find($request->role_id);
//        $user->assignRole($role);

        return response()->json(['status' => 'success', 'message' => 'Record saved successfully']);
    }

    public function delete(Request $request)
    {
        $input = $request->only('id');
        $validator = Validator::make($input, [
            'id' => 'required|integer|min:1',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }
        $user = User::find($input['id']);
        if (!$user) {
            return response()->json(['status' => 'error', 'message' => 'User not found']);
        }
        $user->delete();
        return response()->json(['status' => 'success', 'message' => 'Record deleted successfully']);
    }
}
