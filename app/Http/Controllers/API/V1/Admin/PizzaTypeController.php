<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Admin\AdminController;

use App\Models\PizzaType;
use App\Repositories\PizzaTypeRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class PizzaTypeController extends AdminController
{
    /**
     * @var PizzaTypeRepository
     */
    private $pizzaTypeRepository;

    public function __construct(PizzaTypeRepository $pizzaTypeRepository)
    {
        parent::__construct();
        $this->pizzaTypeRepository = $pizzaTypeRepository;
    }

    public function get(Request $request)
    {

        $pizzaTypes = $this->pizzaTypeRepository->get($request->all());

        return $this->responseSuccess([
            'pizzaTypes' => $pizzaTypes,
            'total' => $pizzaTypes->count(),
        ]);
    }


    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pizza_id' => 'required|integer',
            'base_price' => 'required|numeric',
            'slice' => 'required|integer|min:1',
            'pizza_size' => 'required|integer|min:1|max:4',
            'order' => 'required|integer|min:1',
            'fee' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }

        $pizzaType = PizzaType::find($request->id);
        if (!$pizzaType) {
            $pizzaType = new PizzaType();
        }

        $data = [
            'pizza_id' => $request->pizza_id,
            'base_price' => $request->base_price,
            'slice' => $request->slice ?? 4,
            'pizza_size' => $request->pizza_size ?? 1,
            'order' => $request->order ?? 1,
            'fee' => $request->fee ?? 0.0
        ];

        $pizzaType->fill($data)->save();

        return response()->json(['status' => 'success', 'message' => 'Record saved successfully']);
    }


    public function delete(Request $request)
    {
        $id = $request->get('id');
        $pizza = PizzaType::find($id);
        $pizza->delete();
        return response()->json(['status' => 'success', 'message' => 'Record deleted successfully']);
    }

}
