<?php

namespace App\Http\Controllers\API\V1\Admin;


use App\Http\Controllers\Admin\AdminController;
use App\Models\Currency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class CurrencyController extends AdminController
{

    public function get(Request $request)
    {
        $query = Currency::query();
        $count = $query->get()->count();

        if($request->has('take')){
            $query->take((int)$request->take)->skip((int)$request->skip);
        }

        $currencies = $query->get();

        return response()->json([
            'currencies' => $currencies,
            'total' => $count,
        ]);
    }

    public function save(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|min:1|max:100',
            'code' => 'required|min:3',
            'symbol' => 'required',
            'exchange_rate' => 'required',
            'format' => 'required',
            'active' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }

        $currency = Currency::find($request->id);
        if (!$currency) {
            $currency = new Currency();
        }

        $data = [
            'name' => $request->name,
            'code' => $request->code,
            'symbol' => $request->symbol,
            'exchange_rate' => $request->exchange_rate,
            'format' => $request->format,
            'active' => $request->active,
        ];

        $currency->fill($data)->save();

        return response()->json(['status' => 'success', 'message' => 'Record saved successfully']);
    }

    public function delete(Request $request)
    {
        $item = Currency::find($request->id);
        $item->delete();
        return response()->json(['status' => 'success', 'message' => 'Record deleted successfully']);
    }

}
