<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Admin\AdminController;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DashboardController extends AdminController
{
    private $userRep;

    public function __construct(UserRepository $userRep)
    {
        parent::__construct();
        $this->userRep = $userRep;
    }

    public function mapStatistic(Request $request)
    {
        $dateFrom = Carbon::parse($request->dateFrom)->setTime(0, 0, 0);
        $dateTo = Carbon::parse($request->dateTo)->setTime(23, 59, 59);


        return response()->json([
            'countries' => [],
        ]);
    }
}
