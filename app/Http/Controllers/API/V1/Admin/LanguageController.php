<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Admin\AdminController;
use App\Models\Language;
use App\Models\Translate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LanguageController extends AdminController
{
    public function get()
    {
        $languages = Language::all();
        return response()->json($languages);
    }

    public function getEnabled()
    {
        $languages = Language::enabled()->get();
        return response()->json($languages);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'locale' => 'required|string|max:10',
            'name' => 'required|string|max:60',
            'order' => 'required|integer|min:0',
            'enabled' => 'required|boolean',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }

        $language = Language::find($request->id);
        if (!$language) {
            $language = new Language();
        }

        $data = [
            'locale' => $request->locale,
            'name' => $request->name,
            'order' => $request->order,
            'enabled' => $request->enabled,
        ];
        $language->fill($data)->save();
        return response()->json(['status' => 'success', 'message' => 'Record saved successfully']);
    }

    public function getTranslations()
    {
        $tranlations = Translate::select('locale', 'group', 'item', 'text')
            ->get()
            ->groupBy('group')
            ->map(function ($group) {
                return $group->groupBy('item');
            })
            ->map(function ($group, $key) {
                return $group->groupBy(function ($i) {
                    $subGroup = stristr($i[0]->item, '.', true);
                    if (!$subGroup) return 'common';
                    return $subGroup;
                })
                    ->map(function ($subGroup) {
                        return $subGroup->map(function ($words) {
                            $langs = [];
                            foreach ($words as $word) {
                                $langs[$word->locale] = $word->text;
                            }
                            return ['i' => $words[0]->item, 'l' => $langs];
                        });
                    });
            });

        $tree = [];
        foreach ($tranlations as $key => $tranlation) {
            $tree[$key] = ['l' => $key, 'c' => [], 'id' => $key];
            foreach ($tranlation as $k => $sub) {
                $tree[$key]['c'][] = ['l' => $k, 'id' => $key . '.' . $k];
            }
        }

        return response()->json(['tree' => $tree, 'translations' => $tranlations]);
    }

    public function saveTranslation(Request $request)
    {

        $specialChars= [
            '&auml;'=>'ä',
            '&uuml;'=>'ü',
            '&ouml;'=>'ö',
            '&Ouml;'=>'Ö',
            '&Auml;'=>'Ä',
            '&Uuml;'=>'Ü',
            '&nbsp;' => ' ',
        ];

        $data = [
            'group' => $request->group,
            'item' => $request->item,
            'text' => ''
        ];

        $validator = Validator::make($data, [
            'group' => 'required|string',
            'item' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }

        foreach ($request->get('languages') as $key => $text) {
            $translate = Translate::where('locale', $key)
                ->where('group', $request->group)
                ->where('item', $request->item)
                ->first();

            if ($request->isNew && $translate) {
                return response()->json(['status' => 'error', 'message' => 'Phrase "' . $request->group . '.' . $request->item . '" is duplicate !']);
            }

            if (!$translate) {
                $translate = new Translate();
                $data['locale'] = $key;
            }
            $data['text'] = str_replace(array_keys($specialChars),array_values($specialChars), $text);
            $translate->fill($data);
            $translate->save();
        }

        return response()->json(['status' => 'success', 'message' => 'Phrase saved successfully']);
    }

    public function DeleteTranslation(Request $request)
    {
        $data = [
            'group' => $request->group,
            'item' => $request->item,
        ];

        $validator = Validator::make($data, [
            'group' => 'required|string',
            'item' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }

        $res = Translate::where('group', $request->group)
            ->where('item', $request->item)
            ->delete();

        if ($res) {
            return response()->json(['status' => 'success', 'message' => 'Phrase deleted successfully']);
        } else {
            return response()->json(['status' => 'error', 'message' => 'Delete Operation Unsuccessful']);
        }

    }
}
