<?php

namespace App\Http\Controllers\Admin;

use App\Models\Article;
use App\Models\Comment;
use App\Models\Language;
use App\Services\Telegram;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class ArticleController extends AdminController
{

    protected $thumbnails;

    public function __construct(Telegram $telegram)
    {
        parent::__construct($telegram);
        $this->thumbnails = ['40/40', '125/125'];
    }

    public function get(Request $request)
    {
        $query = Article::with('languages', 'comments');
        $count = $query->get()->count();

        if($request->has('take')){
            $query->take((int)$request->take)->skip((int)$request->skip);
        }

        $articles = $query->get();

        /** @var Article $article */
        foreach ($articles as $article) {
            $article->related = $article->getRelated();
        }

        return response()->json([
            'articles' => $articles,
            'total' => $count,
        ]);
    }

    public function getPossibleRelatedArticles(Request $request)
    {
        $items = Article::select('id', 'img', 'alias')
            ->where('id', '<>', $request->get('id'))
            ->get();
        foreach ($items as $item) {
            unset($item->content);
        }
        return response()->json([
            'items' => $items,
        ]);
    }

    public function publish(Request $request)
    {
        $article = Article::find($request->id);
        $article->published_at = Carbon::now();
        $article->save();
        return response()->json([
            'status' => 'success',
            'message' => 'Article published successfully',
            'published_at' => $article->published_at->format('Y-m-d H:i:s')
        ]);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|integer|min:1',
            'alias' => 'required|string|max:100',
            'meta_title' => 'required|string|max:255',
            'meta_description' => 'required|string|max:255',
            'img' => 'sometimes|required|image|max:2048',
            'imgName' => 'required|string|max:255',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }

        $article = Article::find($request->id);
        if (!$article) {
            $article = new Article();
        }

        $data = [
            'alias' => trim(preg_replace('/(\s|[^A-Za-z0-9\-])+/', '-', $request->alias), '-'),
            'type' => $request->type,
            'meta_title' => $request->meta_title,
            'meta_description' => $request->meta_description,
            'meta_keywords' => $request->meta_keywords,
            'published_at' => $request->published_at,
            'category_id' => $request->category_id ?? 0
        ];
        $article->fill($data)->save();

        $languages = json_decode($request->get('languages'));
        $article->languages()->detach();

        foreach ($languages as $language) {
            $title = '';
            $desc = '';
            $content = '';
            $img_alt = '';
            $tag = '';
            if ($language->pivot) {
                if ($language->pivot->title) {
                    $title = $language->pivot->title;
                }

                if ($language->pivot->desc) {
                    $desc = $language->pivot->desc;
                }

                if ($language->pivot->content) {
                    $content = $language->pivot->content;
                }
                if ($language->pivot->img_alt) {
                    $img_alt = $language->pivot->img_alt;
                }
                if ($language->pivot->tag) {
                    $tag = $language->pivot->tag;
                }

            }
            $data = [
                'title' => $title,
                'desc' => $desc,
                'content' => $content,
                'img_alt' => $img_alt,
                'tag' => (!is_null($tag)) ? $tag : '',
            ];
            $article->languages()->attach($language->id, $data);
        }

        $article->deleteAllRelated();
        $article->saveRelated(json_decode($request->get('relatedIds')));


        if ($request->hasFile('img')) {

            $path = 'news/' . $article->id;
            $filenamewithextension = $request->file('img')->getClientOriginalName();

            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

            //get file extension
            $extension = $request->file('img')->getClientOriginalExtension();

            //filename to store
            $filenametostore = $filename . '.' . $extension;

            $smallthumbnail = 'small_' . $filename . '.' . $extension;

            //Upload File
            $request->file('img')->storeAs($path, $filenametostore, ['disk' => 'public_uploads']);
            $request->file('img')->storeAs($path, $smallthumbnail, ['disk' => 'public_uploads']);

            //create small thumbnail
            $smallthumbnailpath = public_path('uploads/news/' . $article->id . '/' . $smallthumbnail);
            $this->createThumbnail($smallthumbnailpath, 300, 300);


            $article->img = $filenametostore;
            $article->save();

//            $path = 'news/' . $article->id;
//            Storage::disk('public_uploads')->delete($path . $article->img);
//            $request->file('img')->storeAs($path, $request->imgName, ['disk' => 'public_uploads']);
//            $article->img = $request->imgName;
//            $article->save();


        }
        return response()->json(['status' => 'success', 'message' => 'Record saved successfully']);
    }


    public function createThumbnail($path, $width, $height)
    {
        $img = Image::make($path)->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($path);
    }



    public function saveRelated(Request $request)
    {
        $ids = $request->get('ids');
        /** @var Article $article */
        $article->saveRelated($ids);
        return response()->json(['status' => 'success', 'message' => 'Related news saved successfully']);
    }

    public function delete(Request $request)
    {
        $id = $request->get('id');

        /** @var Article $article */
        $article = Article::find($id);
        $article->languages()->detach();
        $article->deleteAllRelated();
        $article->delete();
        return response()->json(['status' => 'success', 'message' => 'Record deleted successfully']);
    }

    public function deleteRelated(Request $request)
    {
        /** @var Article $article */
        $article = Article::find($request->get('id'));
        $article->deleteRelated($request->get('relate_id'));
    }

    public function getLanguages()
    {
        $languages = Language::select('id', 'locale', 'name')->enabled()->get()->toArray();
        foreach ($languages as $key => $language) {
            $languages[$key]['pivot']['title'] = '';
            $languages[$key]['pivot']['desc'] = '';
            $languages[$key]['pivot']['content'] = '';
            $languages[$key]['pivot']['img_alt'] = '';
        }
        return response()->json($languages);
    }

    public function publishComment(Request $request)
    {
        $comment = Comment::find($request->id);
        $comment->published_at = Carbon::now();
        $comment->save();
        return response()->json([
            'status' => 'success',
            'message' => 'Comment published successfully',
            'published_at' => $comment->published_at->format('Y-m-d H:i:s')
        ]);
    }

    public function deleteComment(Request $request)
    {
        /** @var Article $bot */
        $comment = Comment::find($request->id);
        $comment->delete();
        return response()->json(['status' => 'success', 'message' => 'Record deleted successfully']);
    }
}
