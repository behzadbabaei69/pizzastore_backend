<?php

namespace App\Http\Controllers\API\V1\Admin;


use App\Http\Controllers\Admin\AdminController;
use App\Models\Language;
use App\Models\Pizza;
use App\Repositories\PizzaRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class PizzaController extends AdminController
{

    protected $thumbnails;
    /**
     * @var PizzaRepository
     */
    private $pizzaRepository;

    public function __construct(PizzaRepository $pizzaRepository)
    {
        parent::__construct();
        $this->thumbnails = ['40/40', '125/125'];
        $this->pizzaRepository = $pizzaRepository;
    }

    public function get(Request $request)
    {
        $pizzas = $this->pizzaRepository->get($request->all());

        return $this->responseSuccess([
            'pizzas' => $pizzas,
            'total' => $pizzas->count(),
        ]);
    }


    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'description' => 'required|string',
            'img' => 'sometimes|required|image|max:2048',
            'imgName' => 'required|string|max:255',
            'ingredient' => 'sometimes|required|string|max:255',
        ]);

        if ($validator->fails()) {
            return $this->responseSuccess(['status' => 'error', 'message' => $validator->errors()->first()]);
        }

        $pizza = Pizza::find($request->id);
        if (!$pizza) {
            $pizza = new Pizza();
        }

        $data = [
            'name' => $request->name,
            'description' => $request->description,
            'ingredient' => $request->ingredient ?? ''
        ];

        $pizza->fill($data)->save();

        if ($request->hasFile('img')) {

            $path = 'pizzas/' . $pizza->id;
            $fileNameWithExtension = $request->file('img')->getClientOriginalName();

            //get filename without extension
            $fileName = pathinfo($fileNameWithExtension, PATHINFO_FILENAME);

            //get file extension
            $extension = $request->file('img')->getClientOriginalExtension();

            //filename to store
            $fileNameToStore = $fileName . '.' . $extension;

            $smallThumbnail = 'small_' . $fileName . '.' . $extension;

            //Upload File
            $request->file('img')->storeAs($path, $fileNameToStore, ['disk' => 'public_uploads']);
            $request->file('img')->storeAs($path, $smallThumbnail, ['disk' => 'public_uploads']);

            //create small thumbnail
            $smallThumbnailPath = public_path('uploads/pizzas/' . $pizza->id . '/' . $smallThumbnail);
            $this->createThumbnail($smallThumbnailPath, 300, 300);

            $pizza->img = $fileNameToStore;
            $pizza->save();

        }
        return $this->responseSuccess(['status' => 'success', 'message' => 'Record saved successfully']);
    }


    public function createThumbnail($path, $width, $height)
    {
        $img = Image::make($path)->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($path);
    }

    public function delete(Request $request)
    {
        $id = $request->get('id');
        $pizza = Pizza::find($id);
        $pizza->delete();
        return $this->responseSuccess(['status' => 'success', 'message' => 'Record deleted successfully']);
    }

}
