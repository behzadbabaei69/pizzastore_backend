<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class AdminController extends BaseController
{
    public function __construct()
    {
    }

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('layouts.admin');
    }

    protected function responseError($errorMessage = 'error')
    {
        return response()->json(['status' => 'error', 'message' => $errorMessage]);
    }

    protected function responseSuccess($data = [], $message = 'success')
    {
        return response()->json(['status' => 'success', 'message' => $message, 'data' => $data]);
    }
}
