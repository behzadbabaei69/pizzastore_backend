<?php

namespace App\Http\Controllers;

class FrontApiBaseController extends Controller
{
    protected function responseError($errorMessage = 'error')
    {
        return response()->json(['status' => 'error', 'message' => $errorMessage]);
    }

    protected function responseSuccess($data = [], $message = 'success')
    {
        return response()->json(['status' => 'success', 'message' => $message, 'data' => $data]);
    }
}
