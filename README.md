<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>



Database Scheme 
''create schema inno_pizzastore character set utf8 collate utf8_general_ci;''

##seeders
For this project, seeders are provided!
UserSeeder,
LanguageSeeder
SuperAdminPermissionSeeder
PermissionTableSeeder
PizzaSeeder
CurrencySeeder

for testing seeder run:
`php artisan test --group=test-seeders`

command:
`php artisan db:seed --class=PizzaSeeder`

but for running all seeds run
`php artisan db:seed`


Please make sure there is `uploads` folder in your public directory

I'm using php 7.4 so please make sure the packages are compatible with your current php version
there might be some packages for working with the images and they needs to be installed on your systems

packages:
sudo apt-get install  php7.4-imap
sudo apt-get install php7.4-gd
sudo apt-get install  php7.4-mcrypt
