<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('user_id');
            $table->decimal('total_price')->default(0);
            $table->decimal('total_fee')->default(0);
            $table->decimal('delivery_cost')->default(0);
            $table->decimal('total_discount')->default(0);
            $table->boolean('is_delivery')->default(false);
            $table->string('tracking_number')->nullable();
            $table->string('delivery_address',512);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
