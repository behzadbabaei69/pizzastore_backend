<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = \Spatie\Permission\Models\Role::where('name', 'super_admin')->first();

        if (!$role) {
            $role = \Spatie\Permission\Models\Role::create(['guard_name' => 'admins', 'name' => 'super_admin', 'description' => 'Super Admin Role']);
        }

        $role->givePermissionTo(
            'Dashboard_index'
        );

        $super_admins = User::whereIn('email', ['admin@gmail.com', 'behzadbabaei69@gmail.com'])->get();
        foreach ($super_admins as $super_admin) {
            $super_admin->assignRole($role);
        }



    }
}
