<?php

use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{

    public function run()
    {
        $items = \App\Models\Language::all();
        if ($items->count()) {
            foreach ($items as $item) $item->delete();
        }
        $items = [
            [
                'name'  =>  'english',
                'locale'  =>  'en',
                'order'  =>  1,
                'enabled'  =>  false
            ],
            [
                'name'  =>  'german',
                'locale'  =>  'de',
                'order'  =>  2,
                'enabled'  => true
            ],

        ];

        foreach ($items as $item){
            $row = new \App\Models\Language($item);
            $row->save();
        }
    }
}
