<?php

use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('currencies')->truncate();

        \Illuminate\Support\Facades\Artisan::call('currency:manage add usd');
        \Illuminate\Support\Facades\Artisan::call('currency:manage add eur');

        $exchangeRate = [
            'USD' => 1,
            'EUR' => 0.921655
        ];

        $currencies = \App\Models\Currency::all();
        foreach ($currencies as $currency) {
            $currency->active = 1;
            $currency->exchange_rate = $exchangeRate[$currency->code];
            $currency->save();
        }

    }
}
