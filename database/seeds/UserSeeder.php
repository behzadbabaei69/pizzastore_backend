<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::truncate();

        $items[] = [
            'first_name'=>'admin',
            'last_name'=>'',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('123456'),
            'role' => \App\Models\User::ROLE_ADMIN
        ];
        $items[] = [
            'first_name'=>'behzad',
            'last_name'=>'babaei',
            'email' => 'behzadbabaei69@gmail.com',
            'password' => Hash::make('123456'),
            'role' => \App\Models\User::ROLE_ADMIN
        ];

        $items[] = [
            'first_name'=>'innoscripta',
            'last_name'=>'tester',
            'email' => 'innoscripta@test.com',
            'password' => Hash::make('123456'),
            'role' => \App\Models\User::ROLE_ADMIN
        ];

        foreach ($items as $item) {
            $row = new \App\Models\User($item);
            $row->save();
        }
    }
}
