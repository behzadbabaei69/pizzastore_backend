<?php

use App\Models\Pizza;
use App\Models\PizzaType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class PizzaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pizzaSeeds = json_decode(file_get_contents(base_path() . '/source_files/pizzas/pizzas.json'));

        $pizzas = Pizza::all();

        if(!File::exists(public_path('uploads/pizzas'))){
            File::makeDirectory(public_path('uploads/pizzas'),0777);
        }

        $pizzasFolder = File::directories(public_path('uploads/pizzas/'));


        foreach ($pizzasFolder as $folder) {
            $deleteStatus = File::deleteDirectory($folder);
        }

        foreach ($pizzas as $pizza) {
            if (File::isDirectory(public_path('uploads/pizzas/' . $pizza->id))) {
                $deleteStatus = File::deleteDirectory(public_path('uploads/pizzas/' . $pizza->id));
            }
        }

        \DB::table('pizzas')->truncate();
        \DB::table('pizza_types')->truncate();


        \Log::info("previous data were deleted");

        foreach ($pizzaSeeds as $seed) {
            $pizza = new Pizza();
            $pizza->name = $seed->name;
            $pizza->img = $seed->img;
            $pizza->description = $seed->description;
            $pizza->ingredient = $seed->ingredient;

            $pizza->save();

            File::makeDirectory(public_path('uploads/pizzas/' . $pizza->id));

            $srcFile = base_path() . '/source_files/pizzas/images/' . $pizza->img;
            $destFile = public_path('uploads/pizzas/' . $pizza->id . '/' . $pizza->img);
            $status = copy($srcFile, $destFile);

            if ($status) {
                $smallThumbnail = 'small_' . $pizza->img;
                $smallThumbnailPath = public_path('uploads/pizzas/' . $pizza->id . '/' . $smallThumbnail);
                $statusThumbnail = copy($srcFile, $smallThumbnailPath);
                if ($statusThumbnail) {
                    $this->createThumbnail($smallThumbnailPath, 300, 300);
                }
            }


            $pizzaSizes = [
                PizzaType::PIZZA_SMALL => ['name' => 'Small', 'slice' => 4, 'order' => 1],
                PizzaType::PIZZA_MEDIUM => ['name' => 'Medium', 'slice' => 6, 'order' => 2],
                PizzaType::PIZZA_LARGE => ['name' => 'Large', 'slice' => 8, 'order' => 3],
                PizzaType::PIZZA_XLARGE => ['name' => 'XLarge', 'slice' => 10, 'order' => 4]
            ];
        }


        $pizzas = Pizza::all();
        foreach ($pizzas as $pizza) {
            $rand = (rand(50, 100) / 10);
            foreach ($pizzaSizes as $key => $size) {
                $ratio = 0.78;
                $pizzaType = new PizzaType();
                $pizzaType->pizza_id = $pizza->id;
                $pizzaType->pizza_size = $key;
                $pizzaType->base_price = ($rand + floatval($ratio * ($size['slice'])));
                $pizzaType->fee = 0;
                $pizzaType->slice = $size['slice'];
                $pizzaType->order = $size['order'];
                $pizzaType->save();

            }
        }

        \Log::info("Seeding is done!");

    }


    public function createThumbnail($path, $width, $height)
    {
        $img = Image::make($path)->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($path);
    }
}
