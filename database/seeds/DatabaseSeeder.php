<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (!File::exists(public_path('uploads'))) {
            File::makeDirectory(public_path('uploads'), 0777);
        }

        if (!File::exists(public_path('uploads/pizzas'))) {
            File::makeDirectory(public_path('uploads/pizzas'), 0777);
        }

        $this->call(UserSeeder::class);
        $this->call(LanguageSeeder::class);

        $this->call(SuperAdminPermissionSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(PizzaSeeder::class);
        $this->call(CurrencySeeder::class);



        \Illuminate\Support\Facades\Artisan::call('passport:install');
        \Illuminate\Support\Facades\Artisan::call('cache:clear');
        \Illuminate\Support\Facades\Artisan::call('config:clear');

    }
}
