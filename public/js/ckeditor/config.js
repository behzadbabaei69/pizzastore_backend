/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    CKEDITOR.config.defaultLanguage = 'en';
    CKEDITOR.config.language = 'en';
    CKEDITOR.config.height = '450px';
    // config.uiColor = '#AADC6E';

    config.contentsCss = '/css/app_for_cke.css';
    config.allowedContent = true;
    config.toolbarGroups = [
        {name: 'document', groups: ['mode', 'document', 'doctools']},
        {name: 'clipboard', groups: ['clipboard', 'undo']},
        {name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing']},
        {name: 'forms', groups: ['forms']},
        {name: 'tools', groups: ['tools']},
        {name: 'styles', groups: ['styles']},
        '/',
        {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
        {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']},
        {name: 'links', groups: ['links']},
        {name: 'insert', groups: ['insert']},
        // '/',
        // { name: 'colors', groups: [ 'colors' ] },
        // { name: 'others', groups: [ 'others' ] },
        // { name: 'about', groups: [ 'about' ] }
    ];

    config.removeButtons = 'Save,NewPage,SelectAll,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CreateDiv,Anchor,Flash,Chart,Styles,About';

    // config.format_tags = 'p;h1;h2;h3';
    // config.autoParagraph = false;
    // config.image_previewText = ' ';
    // config.enterMode = CKEDITOR.ENTER_BR;
    // config.shiftEnterMode = CKEDITOR.ENTER_BR;

    config.extraPlugins = 'youtube,videoembed,videodetector,btgrid,html5video,widget,widgetselection,clipboard,lineutils';
    //config.extraPlugins = 'widget,uploadwidget,uploadimage,lineutils,clipboard,dialog,dialogui,widgetselection,filetools,notificationaggregator,notification,toolbar,button';
    config.filebrowserImageBrowseUrl = '/ckfinder/browser';
    config.filebrowserImageUploadUrl = '/ckfinder/connector?command=QuickUpload&type=Images&responseType=json';
    config.filebrowserBrowseUrl = '/ckfinder/browser';
    // config.filebrowserUploadUrl = '/upload/upload?type=Files&_token=';
    config.filebrowserUploadUrl = '/ckfinder/connector?command=QuickUpload&type=Images&responseType=json';
    CKEDITOR.on('dialogDefinition', function (ev) {
        var editor = ev.editor;
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;

        if (dialogName == 'image') {
            var infoTab = dialogDefinition.getContents('info');
            infoTab.remove('txtBorder'); //Remove Element Border From Tab Info
            infoTab.remove('txtHSpace'); //Remove Element Horizontal Space From Tab Info
            infoTab.remove('txtVSpace'); //Remove Element Vertical Space From Tab Info
            infoTab.remove('txtWidth'); //Remove Element Width From Tab Info
            infoTab.remove('txtHeight'); //Remove Element Height From Tab Info

            //Remove tab Link
            dialogDefinition.removeContents('Link');
            dialogDefinition.removeContents('advanced');
        }
        if (dialogName == 'link') {
            var infoTab = dialogDefinition.getContents('info');
            dialogDefinition.removeContents('target');
            dialogDefinition.removeContents('advanced');
        }
    });
    config.entities = false;
    config.htmlEncodeOutput = false;
    config.extraPlugins = 'pastetext';
    config.disableNativeSpellChecker = false;
};

