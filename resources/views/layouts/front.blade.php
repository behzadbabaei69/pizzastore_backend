<!DOCTYPE html>
<!--[if IE 7]>
<html lang="en" class="ie7 responsive"
      dir="ltr"> <![endif]-->
<!--[if IE 8]>
<html lang="en" class="ie8 responsive"
      dir="ltr"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 responsive"
      dir="ltr"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="responsive" dir="ltr">
<!--<![endif]-->

<head>

    {{--    <title>@yield('title', trans('title.' . ((Route::current()->getName()=='home') ? (Route::current()->getName()):(Route::current()->getName())))) </title>--}}
    <title>@yield('title', trans('title.home')) </title>
    <meta name="description" content="@yield('meta-description',trans('meta-description.home'))"/>
    <meta name="keywords" content="@yield('meta-keywords',trans('keyword.home'))">
    <link href="{{asset('img/logo.png')}}" rel="icon">

    @include('partials.head')
    @yield('custome_header')



</head>
<body class="app">
<div class="page-container">

    @include('partials.header_navbar')

    <main class="main-content bgc-grey-100">
        <div id="mainContent">
            @yield('content')
        </div>
    </main>

    <h3>Currency exchange</h3>
    <input type="text" value="{{\currency()->getUserCurrency()}}">
    <h3>Currency session</h3>
    <input type="text" value="{{session('currency')}}">
    @include('partials.footer')

</div>


@yield('scripts')

</body>
</html>
