import cookie from "js-cookie";

import {api} from '@/api'
import router from "../router";

export default {
    state: {
        user: {
            id: null,
            email: null,
            role: 0,
            permissions: []
        },
    },
    mutations: {
        setUser(state, payload) {
            state.user.id = payload.id
            state.user.email = payload.email
            state.user.role = payload.role
        },
        setLogout(state) {
            state.user.id = null
            state.user.email = null
            state.user.permissions = []
            cookie.remove('accessToken')
            router.push({name: 'login'})
        },
        setUserPermissions(state, payload) {
            state.user.permissions = payload
        },
        setToken(state, payload) {
            cookie.set('accessToken', payload)
        }
    },
    actions: {
        login({commit}, {email, password}) {
            return new Promise((resolve, reject) => {
                api.post('login', {email, password})
                    .then(response => {
                        if (response.data.status === 'success') {
                            /*if (response.data.user.role !== 2) {
                                reject("Access denied")
                            }*/
                            commit('setToken', response.data.accessToken)
                            commit('setUser', response.data.user)
                            commit('setUserPermissions', response.data.userPermissions)
                            api.defaults.headers['Authorization'] = 'Bearer ' + response.data.accessToken;
                            resolve();
                        }
                        reject(response.data.message)
                    })
                    .catch(e => {
                        console.log(e)
                        reject('error')
                    })
            })
        },
        checkToken({commit}) {
            return new Promise((resolve, reject) => {
                api.post('check-token')
                    .then(response => {
                        if (response.data.status === 'success') {
                            commit('setUser', response.data.user)
                            commit('setUserPermissions', response.data.userPermissions)
                            resolve(response.data.user);
                        }
                        reject(response.data.message)
                    })
                    .catch(e => {
                        console.log(e)
                        reject('error')
                    })
            })
        },
        logout({commit}) {
            commit('setLogout');
        }
    },
    getters: {
        user(state) {
            return state.user
        },
        isAdmin(state) {
            return state.user.role === 2;
        },
        accessToken() {
            return cookie.get('accessToken')
        },

    }
}

