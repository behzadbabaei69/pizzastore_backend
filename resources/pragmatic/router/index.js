import Vue from 'vue'
import Router from 'vue-router'

import Dashboard from '@/pages/Dashboard.vue'


import Translations from '@/pages/languages/Translations'
import Languages from '@/pages/languages/Languages'

// import Admins from '@/pages/settings/Admins'
import Currency from '@/pages/settings/Currency'
import Order from '@/pages/Order'

// import Permission from '@/pages/Permission'
// import Role from '@/pages/Role'

import Login from '@/pages/Login.vue'
import NotFound from '@/pages/NotFound.vue'

import Pizza from '@/pages/Pizza'
import PizzaType from '@/pages/PizzaType'




import layouts from '../layout'
import store from '../store'

Vue.use(Router)

const router = new Router({
    base: '/admin',
    // mode: 'history',
    routes: [
        {
            path: '/',
            alias: '/dashboard',
            name: 'dashboard',
            component: Dashboard,
            meta: {
                auth: true,
                layout: layouts.navLeft,
                searchable: true,
                tags: ['app']
            }
        },
        {path: '/order', component: Order,},
        {path: '/pizza', component: Pizza,},
        {path: '/PizzaType', component: PizzaType,},
        {path: '/languages', component: Languages,},
        {path: '/translations', component: Translations,},
        {path: '/settings/currency', component: Currency,},
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: {
                layout: layouts.contenOnly
            }
        },
        {
            path: '/logout',
            name: 'logout',
            beforeEnter(to, from, next) {
                store.dispatch('logout')
                next({path: '/login'})
            }
        },
        {
            path: '*',
            name: 'not-found',
            component: NotFound,
            meta: {
                layout: layouts.contenOnly
            }
        }
    ]
})


const l = {
    contenOnly() {
        store.commit('setLayout', layouts.contenOnly)
    },
    navLeft() {
        store.commit('setLayout', layouts.navLeft)
    },
    navRight() {
        store.commit('setLayout', layouts.navRight)
    },
    navTop() {
        store.commit('setLayout', layouts.navTop)
    },
    navBottom() {
        store.commit('setLayout', layouts.navBottom)
    },
    set(layout) {
        store.commit('setLayout', layout)
    }
}


router.beforeEach((to, from, next) => {

    if (store.getters.user.id) {
        if (['comfort-transfers', 'accept-transfers', 'bought-transfers', 'not-found', 'login', 'logout'].indexOf(to.name) === -1 && !store.getters.isAdmin) {
            router.push({name: 'comfort-transfers'})
        } else {
            if (to.name === 'login') {
                router.push({name: 'dashboard'})
            } else {
                next()
            }
        }
    } else {
        store.dispatch('checkToken')
            .then(() => {
                next()
            })
            .catch(() => {
                if (to.name !== 'login') {
                    router.push({name: 'login'})
                }
                next()
            })
    }

    if (to && to.meta && to.meta.layout) {
        l.set(to.meta.layout)
    }
})

router.afterEach((to, from) => {
    setTimeout(() => {
        store.commit('setSplashScreen', false)
    }, 500)
})

export default router
