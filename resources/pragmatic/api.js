import axios from 'axios';
import cookie from 'js-cookie';

const token = 'Bearer ' + cookie.get('accessToken')
export const api = axios.create({
    baseURL: `/api/v1/admin`,
    headers: {
        Authorization: token
    }
})
